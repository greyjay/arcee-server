#!/usr/bin/python
# -*- coding: UTF-8 -*-

from custolib_mqtt import deviceMqtt
from custolib_query import Query
from custolib_utils import custolib_utils
# from custolib_AWS_SDK import AWS_SDK
# from custolib_boardUtils import Board_Utils
from datetime import datetime
import ConfigParser
import re
import time
import sdnotify
import os
import json
from custolib_query import Query
from firebase import firebase

firebase = firebase.FirebaseApplication(
    'https://miss-quick.firebaseio.com/', None)
Config = ConfigParser.ConfigParser()


class bootStrap:
    def __init__(self):
        self.sourcepath = "./"
        self.n = sdnotify.SystemdNotifier()
        self.Query = Query()
        self.custolib_utils = custolib_utils()
        Config.read(self.sourcepath + "configfile.ini")
        try:
            inverterstatus = Config.getboolean(
                "ConfigCheck", "InverterConfigStatus")
            self.thingId = Config.get(
                "InverterIdentity", "thingid")
        except ConfigParser.NoSectionError:
            os.system(
                "cp ./defaultconfigfile.ini ./configfile.ini")
            Config.read(self.sourcepath + "configfile.ini")
            inverterstatus = Config.getboolean(
                "ConfigCheck", "InverterConfigStatus")
        if inverterstatus == True:
            self.InverterId = Config.get("InverterIdentity", "InverterID")
            ThingSetupStatus = Config.getboolean(
                "ConfigCheck", "ThingConfigStatus")
            confirmed = False
            while confirmed != True:
                k = "echo %s > ./File" % str(
                    datetime.now())
                os.system(k)
                IDCheck = Query().run_query("QID")
                print IDCheck
                IDCheck = IDCheck[1:]
                IDCheck = re.findall('\\d+', IDCheck)
                IDCheck = "".join(IDCheck)
                if len(IDCheck) >= 9:
                    if self.InverterId == IDCheck and ThingSetupStatus == True:
                        print "calling mqtt callbacks"
                        confirmed = True
                        self.mqttCallbacks()
                    else:
                        print "first inverter setup"
                        confirmed = True
                        self.inverterSetup()
                else:
                    continue
        else:
            print "second inverter setup"
            self.inverterSetup()

    def inverterSetup(self):
        Config.read(self.sourcepath + "configfile.ini")
        #inverterstatus = Config.getboolean("ConfigCheck","InverterConfigStatus")
        inverterstatus = False
        while inverterstatus == False:
            k = "echo %s > ./File" % str(datetime.now())
            os.system(k)
            self.n.notify("EXTEND_TIMEOUT_USEC=60000000")
            result = Query().run_query("QID")
            print result
            result = result[1:]
            result = re.findall('\\d+', result)
            result = "".join(result)
            if len(result) >= 9:
                try:
                    cfgfile = open(self.sourcepath + "configfile.ini", 'w')
                    Config.set("InverterIdentity", "InverterID", result)
                    Config.set("ConfigCheck", "InverterConfigStatus", "True")
                    Config.set("ConfigCheck", "thingconfigstatus", "False")
                    Config.set("SettingsFlag", "settingsflagstatus", "False")
                    Config.set("shadowFlag", "sflag", "False")
                    Config.write(cfgfile)
                    cfgfile.close()
                    time.sleep(0.5)
                    Config.read(self.sourcepath + "configfile.ini")
                    inverterstatus = Config.getboolean(
                        "ConfigCheck", "InverterConfigStatus")
                    print "inverter id set"
                except NoSectionError:
                    os.system(
                        "cp ./defaultconfigfile.ini ./configfile.ini")
                except:
                    print "error writing to configfile"
            else:
                continue
                self.InverterId = Config.get("InverterIdentity", "InverterID")
        print self.InverterId
        self.MqttHandle = deviceMqtt(self.InverterId, self.thingId)
        self.foreverLoop(self.InverterId, self.thingId)
        # self.thingCreateHandle = AWS_SDK(self.InverterId)
        # ThingSetupStatus = Config.getboolean("ConfigCheck","ThingConfigStatus")
        # while ThingSetupStatus == False:
        #   self.n.notify("EXTEND_TIMEOUT_USEC=60000000")
        #   k = "echo %s > ./File" % str(datetime.now())
        #   os.system(k)
        #   self.thingCreateHandle.thingSetup()
        #   Config.read(self.sourcepath + "configfile.ini")
        #   ThingSetupStatus = Config.getboolean("ConfigCheck","ThingConfigStatus")
        #   print "thing status:", ThingSetupStatus

    def mqttCallbacks(self):
        self.MqttHandle = deviceMqtt(self.InverterId, self.thingId)
        # self.settingsUpdate()

    def settingsUpdate(self):
        Config.read(self.sourcepath + "configfile.ini")
        defaultflag = Config.getboolean("SettingsFlag", "settingsflagstatus")
        if defaultflag == False:
            self.MqttHandle.defaultSettings()
            try:
                cfgfile = open(self.sourcepath + "configfile.ini", 'w')
                Config.set("SettingsFlag", "settingsflagstatus", "True")
                Config.write(cfgfile)
                cfgfile.close()
            except:
                print "error writing to config file"
        else:
            pass
        self.MqttHandle.currentSettings()
        self.MqttHandle.requestexpdate()
        self.MqttHandle.programVersion()
        self.n.notify("READY=1")
        self.foreverLoop()

    def foreverLoop(self, InverterId, thingId):
        count = 0
        # self.mqttCallbacks()
        while True:
            parameters = self.Query.parameters(InverterId)
            mode = self.Query.mode(InverterId)
            warning = self.Query.warning(InverterId)
            state = self.Query.inverterState(InverterId)

            payload = {
                "parameters": parameters,
                "mode": mode,
                "warning": warning,
                "state": state
            }
            dayKey = self.custolib_utils.getdaykey()
            timeKey = self.custolib_utils.gettimekey()
            # dataKey = '/grey/data/'+timeKey,
            #print json.dumps(payload)
            firebase.put(
                '/grey/data/'+dayKey, timeKey, payload)
            self.MqttHandle.client.publish(
                '/grey/parameters/', json.dumps(payload), 1)
            print dayKey
            # Config.read(self.sourcepath + "configfile.ini")
            # nextexpirydate = Config.get("ExpiryDate","expirydate")
            # nextexpirydate = datetime.strptime(nextexpirydate, '%Y-%m-%dT%H:%M:%S')
            # adminstatus = Config.getboolean("AdminTurnOff","adminturnoff")
            # userturnoff = Config.getboolean("UserTurnOff","userturnoff")
            # if nextexpirydate >= datetime.now():
            #   if adminstatus == True:
            #       self.Board_Utils.outputRelay('off')
            #       self.Board_Utils.subscriptionLED('on')
            #   elif (adminstatus == False) and (userturnoff == False):
            #       self.Board_Utils.outputRelay('on')
            #       self.Board_Utils.subscriptionLED('on')
            #   elif (adminstatus == False) and (userturnoff == True):
            #       self.Board_Utils.outputRelay('off')
            #       self.Board_Utils.subscriptionLED('on')
            #   else:
            #       pass
            # else:
            #   self.Board_Utils.outputRelay('off')
            #   self.Board_Utils.subscriptionLED('off')
            # #print "loop"
            # self.n.notify("WATCHDOG=1")
            # if count < 10:
            #   k = "echo %s >> ./File" % str(datetime.now())
            #   os.system(k)
            #   count = count + 1
            # else:
            #   k = "echo %s > ./File" % str(datetime.now())
            #   os.system(k)
            #   count = 0
            time.sleep(45)


firecustolib = bootStrap()
