import os, time
import ConfigParser
Config = ConfigParser.ConfigParser()
# Config.read("configfile.ini")

class Board_Utils:
	def __init__(self):
		self.sourcepath = "/home/holhujay/dev/src/"
		self.relaySet = "/sys/class/gpio/pioC18/value"
		self.relayUnSet = "/sys/class/gpio/pioC26/value"
		self.subLedPath = "/sys/class/gpio/pioC20/value"

	def setRelay(self):
		try:
			ruset = open(self.relayUnSet,"w" )
			ruset.write("0")
			time.sleep(1)
			ruset.close()
			rset = open(self.relaySet,"w" )
			rset.write("1")
			time.sleep(1)
			rset.write("0")
			rset.close()
		except:
			print "could not set relay pin"	

	def unSetRelay(self):
		try:
			rset = open(self.relaySet,"w" )
			rset.write("0")
			time.sleep(1)
			rset.close()
			ruset = open(self.relayUnSet,"w" )
			ruset.write("1")
			time.sleep(1)
			ruset.write("0")
			ruset.close()
		except:
			print "could not unset relay pin"

	def relayInit(self):
		Config.read(self.sourcepath + "configfile.ini")
		flagCheck = Config.get("RelayFlag", "relayflag")
		if flagCheck == "on":
			self.setRelay()
		elif flagCheck == "off":
			self.unSetRelay()

	def outputRelay(self, switch):
		print switch
		Config.read(self.sourcepath + "configfile.ini")
		flagCheck = Config.get("RelayFlag", "relayflag")
		if switch == 'on' and flagCheck == 'off':
			try:
				self.setRelay()
				cfgfile = open(self.sourcepath + "configfile.ini",'w')
				Config.set("RelayFlag","relayflag",'on')
				Config.write(cfgfile)
				cfgfile.close()
				print "system on"
			except:
				print "could not change relay GPIO value"

		elif switch == 'off' and flagCheck == 'on':
			try:
				self.unSetRelay()
				cfgfile = open(self.sourcepath + "configfile.ini",'w')
				Config.set("RelayFlag","relayflag",'off')
				Config.write(cfgfile)
				cfgfile.close()
				print "system off"
			except:
				print "could not change relay GPIO value"
			
		else:
			pass

	def RebootCommand(self):
		os.system('reboot')

	def ShellCommand(self, command):
		os.system(command)
		

	def ResetCommand(self):
		os.system(self.sourcepath + "reset.sh")

	def SSHCommand(self, shell):
		shell = shell.split(' ')
		var1 = shell[0]
		var2 = shell[1]
		var3 = shell[2]
		instruct = self.sourcepath + "reversessh.exp {0} {1} {2}".format(var1,var2,var3)
		os.system(instruct)

	def subscriptionLEDinit(self):
		Config.read(self.sourcepath + "configfile.ini")
		flagCheck = Config.get("LedFlag", "Ledflagstatus")
		if flagCheck == "on":
			try:
				led = open(self.subLedPath,"w" )
				led.write("1")
        			led.close()
			except:
				print "could not turn on sub led"			
		elif flagCheck == "off":
			try:
				led = open(self.subLedPath,"w" )
				led.write("0")
        			led.close()
			except:
				print "could not turn off sub led"	
		else:
			pass

	def subscriptionLED(self, indicator):
		flagCheck = Config.get("LedFlag", "Ledflagstatus")
		if indicator == 'on' and flagCheck == 'off':
			try:
				led = open(self.subLedPath,"w" )
				led.write("1")
        			led.close()
				cfgfile = open(self.sourcepath + "configfile.ini",'w')
				Config.set("LedFlag","Ledflagstatus",'on')
				Config.write(cfgfile)
				cfgfile.close()
			except:
				print "could not change led GPIO value"
		
		elif indicator == 'off' and flagCheck == 'on':
			try:
				led = open(self.subLedPath,"w" )
				led.write("0")
        			led.close()
				cfgfile = open(self.sourcepath + "configfile.ini",'w')
				Config.set("LedFlag","Ledflagstatus",'off')
				Config.write(cfgfile)
				cfgfile.close()
			except:
				print "could not change led GPIO value"
		else:
			pass
