import sqlite3, time, json
from datetime import datetime
from custolib_parser import parser


class energyData:
	def __init__(self):	
		self.parser = parser()

	def eg3_for(self, keyParameters, valueParameters):
		jsonForm = {}
		for i in range(len(keyParameters)):
			jsonForm[keyParameters[i]] = valueParameters[i]
		return jsonForm

	def createDB(self):
		print "......creating tables......"
		self.c.execute('''CREATE TABLE energyData (
				ID INT PRIMARY KEY,
				datestamp text, 
				generatedToday REAL,
				generatedWeek REAL,
				generatedMonth REAL,
				generatedYear REAL,
				consumedTodayongrid REAL,
				consumedTodayoffgrid REAL,
				consumedWeekongrid REAL,
				consumedWeekoffgrid REAL,
				consumedMonthongrid REAL,
				consumedMonthoffgrid REAL,
				consumedYearongrid REAL,
				consumedYearoffgrid REAL,
				peakToday REAL,
				peakWeek REAL,
				peakMonth REAL,
				peakYear REAL);''')
		print "Tables created"
		print "Inserting values"
		self.c.execute("INSERT INTO energyData(ID, datestamp, generatedToday, generatedWeek, generatedMonth, generatedYear, consumedTodayongrid, consumedTodayoffgrid, consumedWeekongrid, consumedWeekoffgrid, consumedMonthongrid, consumedMonthoffgrid, consumedYearongrid, consumedYearoffgrid, peakToday, peakWeek, peakMonth, peakYear) \
			VALUES (1, '2010-01-01', 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)");
		self.conn.commit()

	def getEnergyData(self, consumedpower, generatedpower, grid):

		grid = float(grid)
		consumedpower = int(consumedpower)
		generatedpower = int(generatedpower)
		generatedKWh = generatedpower/60000.0
		consumedKWh = consumedpower/60000.0
		instantPower = consumedpower/1000.0
		
		try:
			self.conn = sqlite3.connect("../data.sqlite")
			self.c = self.conn.cursor()
			cur = self.c.execute("SELECT datestamp from energyData WHERE ID = 1")
			for row in cur:
				predate = row[0]

			predate = str(predate)
			date = datetime.strptime(predate, '%Y-%m-%d')
			preweek = datetime.date(date).isocalendar()[1]
			datenow = datetime.now()#.date()
			currentDate = datenow.day
			predate = date.day
			currentWeek = datetime.date(datenow).isocalendar()[1]
			premonth = date.month
			currentMonth = datenow.month
			preyear = date.year
			currentYear = datenow.year

			if predate == currentDate:
				''' aggregate the sum of daily kwh'''
				#print "should update",generatedKWh
				self.c.execute("UPDATE energyData SET generatedToday = generatedToday + %f WHERE ID = 1"% generatedKWh)
				self.c.execute("UPDATE energyData SET peakToday = %(0)f WHERE ID = 1 AND peakToday < %(0)f" % {"0":instantPower})
				if grid > 110:
					self.c.execute("UPDATE energyData SET consumedTodayongrid = consumedTodayongrid + %f WHERE ID = 1"% consumedKWh)
				else:
					self.c.execute("UPDATE energyData SET consumedTodayoffgrid = consumedTodayoffgrid + %f WHERE ID = 1"% consumedKWh)

			else:
				#overwrite the value that was there before with a new value
				self.c.execute("UPDATE energyData SET generatedToday = 0.0 WHERE ID = 1")
				self.c.execute("UPDATE energyData SET consumedTodayongrid =  0.0 WHERE ID = 1")
				self.c.execute("UPDATE energyData SET consumedTodayoffgrid =  0.0 WHERE ID = 1")
				self.c.execute("UPDATE energyData SET peakToday = 0.0 WHERE ID = 1")
			
			if preweek == currentWeek:
				#add current value to week so far value
				self.c.execute("UPDATE energyData SET generatedWeek = generatedWeek + %f WHERE ID = 1"% generatedKWh)
				if grid > 110:
					self.c.execute("UPDATE energyData SET consumedWeekongrid = consumedWeekongrid + %f WHERE ID = 1"% consumedKWh)
				else:
					self.c.execute("UPDATE energyData SET consumedWeekoffgrid = consumedWeekoffgrid + %f WHERE ID = 1"% consumedKWh)
				self.c.execute("UPDATE energyData SET peakWeek = %(0)f WHERE ID = 1 AND peakWeek < %(0)f" % {"0":instantPower})

			else:
				# overwrite week so far value
				self.c.execute("UPDATE energyData SET generatedWeek = 0.0 WHERE ID = 1")
				self.c.execute("UPDATE energyData SET consumedWeekongrid = 0.0 WHERE ID = 1")
				self.c.execute("UPDATE energyData SET consumedWeekoffgrid = 0.0 WHERE ID = 1")
				self.c.execute("UPDATE energyData SET peakWeek = 0.0 WHERE ID = 1")

			if premonth == currentMonth:
				#add current value to month so far value
				self.c.execute("UPDATE energyData SET generatedMonth = generatedMonth + %f WHERE ID = 1"% generatedKWh)
				if grid > 110:
					self.c.execute("UPDATE energyData SET consumedMonthongrid = consumedMonthongrid + %f WHERE ID = 1"% consumedKWh)
				else:
					self.c.execute("UPDATE energyData SET consumedMonthoffgrid = consumedMonthoffgrid + %f WHERE ID = 1"% consumedKWh)
				self.c.execute("UPDATE energyData SET peakMonth = %(0)f WHERE ID = 1 AND peakMonth < %(0)f" % {"0":instantPower})

			else:
				#overwrite month so far value
				self.c.execute("UPDATE energyData SET generatedMonth = 0.0 WHERE ID = 1")
				self.c.execute("UPDATE energyData SET consumedMonthongrid = 0.0 WHERE ID = 1")
				self.c.execute("UPDATE energyData SET consumedMonthoffgrid = 0.0 WHERE ID = 1")
				self.c.execute("UPDATE energyData SET peakMonth = 0.0 WHERE ID = 1")

			if preyear == currentYear:
				#add current value to year so far value
				self.c.execute("UPDATE energyData SET generatedYear = generatedYear + %f WHERE ID = 1"% generatedKWh)
				if grid > 110:
					self.c.execute("UPDATE energyData SET consumedYearongrid = consumedYearongrid + %f WHERE ID = 1"% consumedKWh)
				else:
					self.c.execute("UPDATE energyData SET consumedYearoffgrid = consumedYearoffgrid + %f WHERE ID = 1"% consumedKWh)
				self.c.execute("UPDATE energyData SET peakYear = %(0)f WHERE ID = 1 AND peakYear < %(0)f" % {"0":instantPower})

			else:
				#overwrite year so far value
				self.c.execute("UPDATE energyData SET generatedYear = 0.0 WHERE ID = 1")
				self.c.execute("UPDATE energyData SET consumedYearongrid = 0.0 WHERE ID = 1")
				self.c.execute("UPDATE energyData SET consumedYearoffgrid = 0.0 WHERE ID = 1")
				self.c.execute("UPDATE energyData SET peakYear = 0.0 WHERE ID = 1")

			newDate = datetime.now().date()
			if predate != newDate:
				#print newDate
				newDate = str(newDate)
				#print newDate
				self.c.execute("UPDATE energyData SET datestamp = '%s' WHERE ID = 1"% newDate)

			self.conn.commit()

			data = self.c.execute("SELECT * from energyData WHERE ID = 1")

			for row in data:
				#print row[0]
				#print row[1]
				genToday = str(round(row[2], 3))
				genWeek = str(round(row[3], 3))
				genMonth = str(round(row[4], 3))
				genYear = str(round(row[5], 3))
				conTodayongrid = str(round(row[6], 3))
				conTodayoffgrid = str(round(row[7], 3))
				conWeekongrid = str(round(row[8], 3))
				conWeekoffgrid = str(round(row[9], 3))
				conMonthongrid = str(round(row[10], 3))
				conMonthoffgrid = str(round(row[11], 3))
				conYearongrid = str(round(row[12], 3))
				conYearoffgrid = str(round(row[13], 3))
				pkToday = str(round(row[14], 3))
				pkWeek = str(round(row[15], 3))
				pkMonth = str(round(row[16], 3))
				pkYear = str(round(row[17], 3))

			self.conn.close()
			data_a = (genToday, genWeek, genMonth, genYear, conTodayongrid, conTodayoffgrid, conWeekongrid, conWeekoffgrid, conMonthongrid, conMonthoffgrid, conYearongrid, conYearoffgrid, pkToday, pkWeek, pkMonth, pkYear)
			data_b = ' '.join(data_a)
			data_c = data_b.split(" ")
			EnergyData = self.eg3_for(self.parser.energyData, data_c)
			#EnergyData = json.dumps(preData, indent = 2)
			return EnergyData
		except sqlite3.Error as err:
			if str(err) == "no such table: energyData":
				print "creating DB"
				self.createDB()
			else:
				print err

		
