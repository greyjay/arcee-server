# -*- coding: UTF-8 -*-
import paho.mqtt.client as mqtt
from custolib_query import Query
from custolib_utils import custolib_utils
from custolib_boardUtils import Board_Utils
from custolib_parser import parser
from custolib_energy import energyData
import time
import ConfigParser
import ssl
import re
import json
from datetime import datetime
Config = ConfigParser.ConfigParser()
# Config.read("configfile.ini")


class deviceMqtt:
    def __init__(self, InverterId, thingId):
        self.sourcepath = "/home/holhujay/dev/src/"
        self.InverterId = InverterId
        self.thingId = thingId
        self.delimeter = " "
        self.errorCode = "500"
        self.notAck = '(NAKss'
        self.exMode = ""
        self.exWarning = ""
        self.count = 0
        self.exParameters = ""
        self.exInverterState = ""
        self.emptyJson = json.dumps('')
        self.JSONPayload = '{"state":{"desired": {"userPower" : "ON", "adminPower" : "ON", "setExpiry" : "2100-01-01T01:01:01", "requestExpiryDate" : "False", "requestCurrentSettings" : "False", "changeInverterSettings" : "NULL", "rebootSystem" : "False","shellCommand" : "NULL", "boardReset" : "False", "uptime":"False" }}, "state":{"reported": {"userPower" : "ON", "adminPower" : "ON", "setExpiry" : "2100-01-01T01:01:01", "requestExpiryDate" : "False", "requestCurrentSettings" : "False", "changeInverterSettings" : "NULL", "rebootSystem" : "False","shellCommand" : "NULL", "boardReset" : "False", "uptime":"False" }}}'
        self.JSONExpiryFalse = '{"state":{"reported": {"requestExpiryDate" : "False"}}, "state":{"desired": {"requestExpiryDate" : "False"}}}'
        self.JSONCurrentSettingsFalse = '{"state":{"reported": {"requestCurrentSettings" : "False"}}, "state":{"desired": {"requestCurrentSettings" : "False"}}}'
        self.JSONInverterSettingsFalse = '{"state":{"reported": {"changeInverterSettings" : "NULL"}}, "state":{"desired": {"changeInverterSettings" : "NULL"}}}'
        self.JSONRebootFalse = '{"state":{"reported": {"rebootSystem" : "False"}}, "state":{"desired": {"rebootSystem" : "False"}}}'
        self.JSONShellFalse = '{"state":{"reported": {"shellCommand" : "NULL"}}, "state":{"desired": {"shellCommand" : "NULL"}}}'
        self.JSONResetFalse = '{"state":{"reported": {"boardReset" : "False"}}, "state":{"desired": {"boardReset" : "False"}}}'
        self.JSONUptimeFalse = '{"state":{"reported": {"uptime" : "False"}}, "state":{"desired": {"uptime" : "False"}}}'
        self.parser = parser()
        self.Query = Query()
        self.Board_Utils = Board_Utils()
        self.client = mqtt.Client(client_id=self.thingId)
        self.host = "mqtt.eclipse.org"
        self.url = "https://{}".format(self.host)
        self.port = 1883
        self.protocol_name = "x-amzn-mqtt-ca"
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.on_publish = self.on_publish
        self.client.on_disconnect = self.on_disconnect
        self.client.on_subscribe = self.on_subscribe
        self.custolib_utils = custolib_utils()
        self.energyData = energyData()
        self.AWSLedPath = "/sys/class/gpio/pioC26/value"
        self.version = "1.2.2"

        try:
            print "Attempting to connect to Broker"
            # ssl_context = self.ssl_alpn()
            # self.client.tls_set_context(context=ssl_context)
            self.client.connect_async(self.host, self.port, 30)
            self.client.loop_start()
        except:
            #logging.warning('cannot connect to network')
            print "unable to connect"

    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            print 'connected with result code ' + str(rc)
            Config.read(self.sourcepath + "configfile.ini")
            Flag = Config.get("shadowFlag", "sflag")
            if Flag == "True":
                # print "inside true"
                pass
            elif Flag == "False":
                # print "inside False"
                self.client.publish(
                    "$aws/things/" + self.InverterId + "/shadow/update", self.JSONPayload, 1)
                cfgfile = open(self.sourcepath + "configfile.ini", 'w')
                Config.set("shadowFlag", "sflag", "True")
                Config.write(cfgfile)
                cfgfile.close()
            else:
                # print "inside pass"
                pass
            time.sleep(1)
            self.client.subscribe(
                "$aws/things/" + self.InverterId + "/shadow/get/accepted", 1)
            self.client.subscribe(
                "$aws/things/" + self.InverterId + "/shadow/update/delta", 1)
            time.sleep(1)
            self.client.publish(
                "$aws/things/" + self.InverterId + "/shadow/get", self.emptyJson, 1)
            try:
                AWSLed = open(self.AWSLedPath, "w")
                AWSLed.write("1")
                AWSLed.close()
                # print "Led ON"
            except:
                print "could not change connected GPIO value"
        else:
            #logging.info('Bad connection with Returned code ' +str(rc))
            print "could not connect to AWS"

    def on_subscribe(self, client, userdata, mid, granted_qos):
        # print 'Succesfully subscribed with QoS of %s', granted_qos
        pass

    def on_publish(self, client, userdata, mid):
        # print "data published"
        pass

    def on_message(self, client, userdata, msg):
        try:
            received = json.loads(msg.payload)
            # print received
            if msg.topic == "$aws/things/" + self.InverterId + "/shadow/get/accepted":
                for msg in received.items():
                    message = received['state']['desired']
                self.processGetMessage(message)
            elif msg.topic == "$aws/things/" + self.InverterId + "/shadow/update/delta":
                for msg in received.items():
                    message = received['state']
                self.processGetMessage(message)
            else:
                pass
        except:
            print "Error in processing received message"
            pass

    def processGetMessage(self, message):

        # print message
        if 'userPower' in message:
            messageData = message['userPower']
            self.UserTurnOffCommand(messageData)

        if 'adminPower' in message:
            messageData = message['adminPower']
            self.AdminTurnOffCommand(messageData)

        if 'setExpiry' in message:
            messageData = message['setExpiry']
            self.expiryDate(messageData)
        if 'requestExpiryDate' in message:
            messageData = message['requestExpiryDate']
            if messageData == 'True':
                self.client.publish(
                    "$aws/things/" + self.InverterId + "/shadow/update", self.JSONExpiryFalse, 1)
                self.requestexpdate()
        if 'requestCurrentSettings' in message:
            messageData = message['requestCurrentSettings']
            if messageData == 'True':
                self.client.publish("$aws/things/" + self.InverterId +
                                    "/shadow/update", self.JSONCurrentSettingsFalse, 1)
                self.currentSettings()

        if 'changeInverterSettings' in message:
            messageData = message['changeInverterSettings']
            if messageData != "NULL":
                self.inverterConfig(messageData)

        if 'rebootSystem' in message:
            messageData = message['rebootSystem']
            if messageData == 'True':
                self.client.publish(
                    "$aws/things/" + self.InverterId + "/shadow/update", self.JSONRebootFalse, 1)
                time.sleep(5)
                self.Board_Utils.RebootCommand()

        if 'shellCommand' in message:
            messageData = message['shellCommand']
            if messageData != "NULL":
                self.client.publish(
                    "$aws/things/" + self.InverterId + "/shadow/update", self.JSONShellFalse, 1)
                resp = self.Board_Utils.ShellCommand(messageData)

        if 'uptime' in message:
            messageData = message['uptime']
            if messageData == 'True':
                self.client.publish(
                    "$aws/things/" + self.InverterId + "/shadow/update", self.JSONUptimeFalse, 1)
                self.SendUptime()

        if 'boardReset' in message:
            messageData = message['boardReset']
            if messageData == 'True':
                self.client.publish(
                    "$aws/things/" + self.InverterId + "/shadow/update", self.JSONResetFalse, 1)
                self.Board_Utils.ResetCommand()

    def on_disconnect(self, client, userdata, rc):
        #logging.info("disconnecting reason  "  +str(rc))
        # print "Disconnected from AWS"
        try:
            AWSLed = open(self.AWSLedPath, "w")
            AWSLed.write("0")
            AWSLed.close()
            # print "led off"
        except Exception, e:
            print e
