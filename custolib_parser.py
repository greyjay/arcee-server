

class parser:
	def __init__(self):
		self.qpigsParameters = ['InverterId', 'TimeStamp', 'Gridvoltage', 'GridFrequency', 'ACOutputVoltage', 'ACOutputFrequency', 'ACOutputApparentPower', 'ACOutputActivePower', 'OutputLoadPercent','BusVoltage', 'BatteryVoltage', 'BatteryChargingCurrent', 'BatteryCapacity', 'InverterHeatSinkTemp', 'PVInputCurrent', 'PVInputVoltage', 'BatteryVoltageFromSCC', 'BatteryDischargeCurrent', 'DeviceStatus1', 'BatteryVoltageOffsetForFan', 'EEPROMVersion', 'PVChargingPower', 'DeviceStatus2' ]
		self.deviceSpecification = [ 'InverterId', 'TimeStamp', 'GridVoltage', 'GridCurrent', 'ACOutputVoltage', 'ACOutputFrequency', 'ACOutputCurrent', 'ACOutputApparentPower', 'ACOutputActivePower', 'BatteryVoltage', 'BatteryRechargeVoltage', 'BatteryUnderVoltage', 'BatteryBulkVoltage', 'BatteryFloatVoltage', 'BatteryType', 'MaxACChargingCurrent', 'MaxChargingCurrent', 'InputVoltage', 'OutputSourcePriority', 'Chargersourcepriority', 'Reserved1', 'MachineType', 'Topology', 'OutputMode', 'Batteryre-dischargeVoltage', 'Reserved2', 'Reserved3', 'Unknown']
		self.deviceWarning = ['InverterId', 'TimeStamp','WarningMessage']
		self.deviceWarningMessage =  ['InverterId', 'TimeStamp', 'PVLoss', 'InverterFault', 'BusOver', 'BusUnder','BusSoftFail','LineFail', 'OPVShort', 'InverterVoltageTooLow', 'InverterVoltageTooHigh', 'OverTemperature', 'FanLocked', 'BatteryVoltageHigh', 'BatteryLowAlarm', 'Reserved01', 'BatteryUnderShutdown', 'BatteryDerating', 'Overload', 'EepromFault', 'InverterOverCurrent', 'InverterSoftFail', 'SelfTestFail', 'OPDCVoltageOpen', 'BatteryOpen', 'CurrentSensorFail', 'Reserved02', 'Reserved03', 'Reserved04', 'Reserved05', 'Reserved06', 'Reserved07', 'Reserved08', 'Reserved09' ]
		self.deviceMode = ['InverterId', 'TimeStamp', 'Mode']
		self.deviceDefaultSetting = ['InverterId', 'TimeStamp', 'ACOutputVoltage','ACOutputFrequency', 'MaxACChargingCurrent', 'BatteryUnderVoltage', 'ChargingFloatVoltage', 'ChargingBulkVoltage', 'BatteryDefaultRechargeVoltage', 'MaxChargingCurrent', 'ACInputVoltageRange', 'OutputSourcePriority', 'ChargerSourcePriority', 'BatteryType', 'IOSilenceBuzzer', 'IOPowerSaving', 'IOOverloadRestart', 'IOOverTemperatureRestart', 'IOLCDBacklightOn', 'IOAlarmOnPrimarySourceInterrupt','IOFaultCodeRecord', 'OverloadBypass', 'IOLCDEscapeToDefaultPageAfter1minTimeOut', 'OutputMode', 'BatteryRedischargeVoltage', 'PVCondition', 'PVPowerBalance']
		self.errorLogs = ['InverterId', 'TimeStamp', 'error']
		self.expiryDate = ['InverterId', 'TimeStamp', 'expirydate']
		self.version = ['InverterId', 'TimeStamp', 'version']
		self.inverterState = ['InverterId', 'TimeStamp', 'inverterState']
		self.boardUptime = ['InverterId', 'BoardUptime']
		self.energyData = ['GeneratedToday', 'GeneratedWeek', 'GeneratedMonth', 'GeneratedYear', 'ConsumedTodayOnGrid', 'ConsumedTodayOffGrid', 'ConsumedWeekOnGrid', 'ConsumedWeekOffGrid', 'ConsumedMonthOnGrid', 'ConsumedMonthOffGrid', 'ConsumedYearOnGrid', 'ConsumedYearOffGrid', 'PeakToday', 'PeakWeek', 'PeakMonth', 'PeakYear']

