import serial
import crcmod
import time
import sys
from custolib_utils import custolib_utils
from binascii import unhexlify
from string import ascii_letters, punctuation, whitespace
import time
import ConfigParser
import ssl
import re
import json
from custolib_parser import parser
from custolib_energy import energyData
from custolib_utils import custolib_utils
ser = serial.Serial('/dev/ttyUSB0', 2400, timeout=3)
# ser = serial.Serial('/dev/ttyUSB0', 2400, timeout=3)
crc = crcmod.mkCrcFun(0x11021, rev=False, initCrc=0x0000, xorOut=0x0000)
Config = ConfigParser.ConfigParser()


def eg3_for(keyParameters, valueParameters):
    jsonForm = {}
    for i in range(len(keyParameters)):
        jsonForm[keyParameters[i]] = valueParameters[i]
    return jsonForm


class Query:
    def __init__(self):
        self.sourcepath = "./"
        self.notAck = '(NAKss'
        self.energyData = energyData()
        self.exMode = ""
        self.exWarning = ""
        self.count = 0
        self.exParameters = ""
        self.exInverterState = ""
        self.custolib_utils = custolib_utils()
        self.parser = parser()
        # selfInverterId = InverterId
        pass

    def parameters(self, InverterId):
        result = self.run_query("QPIGS")
        time.sleep(1)
        # print result
        if len(result) > 0:  # add check condition and publish
            if result != self.notAck:
                try:
                    pattern = re.compile(
                        r'(\d{3}\.\d\s\d{2}\.\d\s){2}(\d{4}\s){2}(\d{3}\s){2}(\d{2}\.\d{2}\s)(\d{3}\s){2}(\d{4}\s){2}(\d{3}\.\d\s)(\d{2}\.\d{2}\s)(\d{5}\s)(\d{8}\s)(\d{2}\s){2}(\d{5}\s)(\d{3})')
                    matches = pattern.search(result)
                    result = matches.group()
                    # if result != self.exParameters:
                    self.exParameters = result
                    timestamp = self.custolib_utils.datatimestamp()
                    result = (InverterId, timestamp, result)
                    result_a = ' '.join(result)
                    result_b = result_a.split(" ")
                    result_c = eg3_for(
                        self.parser.qpigsParameters, result_b)
                    result_d = json.dumps(result_c, indent=2)
                    data = json.loads(result_d)
                    consumed = data['ACOutputActivePower']
                    generated = data['PVChargingPower']
                    grid = data['Gridvoltage']
                    energydata = self.energyData.getEnergyData(
                        consumed, generated, grid)
                    # print energydata
                    data["EnergyData"] = energydata
                    # final_result = json.dumps(data)
                    # print final_result
                    return data
                    # print '/grey/parameters/'+hookKey
                except Exception, e:
                    print e
                    pass
        else:
            timestamp = self.custolib_utils.datatimestamp()
            errorId = (InverterId, timestamp, self.errorCode)
            errorId_a = ' '.join(errorId)
            errorId_b = errorId_a.split(" ")
            errorId_c = eg3_for(self.parser.errorLogs, errorId_b)
            errorId_d = json.dumps(errorId_c, indent=2)
            print errorId_d
            # # self.client.publish('data/errorlogs/v2', errorId_d, 1)

    def mode(self, InverterId):
        result = self.run_query("QMOD")
        time.sleep(1)
        if len(result) > 0:  # add check condition and publish
            if result != self.notAck:
                try:
                    pattern = re.compile(r'([A-Z])')
                    matches = pattern.search(result)
                    result = matches.group()
                    # if result != self.exMode:
                    self.exMode = result
                    timestamp = self.custolib_utils.datatimestamp()
                    result = (InverterId, timestamp, result)
                    result_a = ' '.join(result)
                    result_b = result_a.split(" ")
                    result_c = eg3_for(
                        self.parser.deviceMode, result_b)
                    # result_d = json.dumps(result_c, indent=2)
                    return result_c
                    # self.client.publish('data/mode/v2', result_d, 1)
                except:
                    pass
        else:
            timestamp = self.custolib_utils.datatimestamp()
            errorId = (InverterId, timestamp, self.errorCode)
            errorId_a = ' '.join(errorId)
            errorId_b = errorId_a.split(" ")
            errorId_c = eg3_for(self.parser.errorLogs, errorId_b)
            errorId_d = json.dumps(errorId_c, indent=2)
            # self.client.publish('data/errorlogs/v2', errorId_d, 1)

    def warning(self, InverterId):
        out = ""
        count = 0
        result = self.run_query("QPIWS")
        time.sleep(1)
        if len(result) > 0:  # add check condition and publish
            if result != self.notAck:
                try:
                    pattern = re.compile(r'(\d{32})')
                    matches = pattern.search(result)
                    result = matches.group()
                    if result:
                        # self.exWarning = result
                        for i in result:
                            out = out + i + " "
                            count = count + 1
                        timestamp = self.custolib_utils.datatimestamp()
                        result = (InverterId, timestamp, out)
                        result_a = ' '.join(result)
                        result_b = result_a.split(" ")
                        result_c = eg3_for(
                            self.parser.deviceWarningMessage, result_b)
                        # result_d = json.dumps(result_c, indent=2)
                        # print result_d
                        # return result_d
                        return result_c
        # self.client.publish('data/warning/v2', result_d, 1)
                except:
                    pass
        else:
            timestamp = self.custolib_utils.datatimestamp()
            errorId = (InverterId, timestamp, self.errorCode)
            errorId_a = ' '.join(errorId)
            errorId_b = errorId_a.split(" ")
            errorId_c = eg3_for(self.parser.errorLogs, errorId_b)
            errorId_d = json.dumps(errorId_c, indent=2)
            print errorId_d
            # self.client.publish('data/errorlogs/v2', errorId_d, 1)

    def inverterState(self, InverterId):
        Config.read(self.sourcepath + "configfile.ini")
        currentInverterSate = Config.get("RelayFlag", "relayflag")
        if currentInverterSate:
            self.exInverterState = currentInverterSate
            timestamp = self.custolib_utils.datatimestamp()
            currentInverterSate = (InverterId, timestamp, currentInverterSate)
            currentInverterSate_a = ' '.join(currentInverterSate)
            currentInverterSate_b = currentInverterSate_a.split(" ")
            currentInverterSate_c = eg3_for(
                self.parser.inverterState, currentInverterSate_b)
            # currentInverterSate_d = json.dumps(currentInverterSate_c, indent=2)
            # print currentInverterSate_d
            return currentInverterSate_c
        # self.client.publish('data/inverterstate/v2', currentInverterSate_d, 1)
        else:
            pass

    def programVersion(self, InverterId):
        timestamp = self.custolib_utils.datatimestamp()  # get current time from the system
        version = (InverterId, timestamp, self.version)
        version_a = ' '.join(version)
        version_b = version_a.split(" ")
        version_c = eg3_for(self.parser.version, version_b)
        version_d = json.dumps(version_c, indent=2)
        self.client.publish('data/version/v2', version_d, 1)

    def defaultSettings(self, InverterId):
        result = self.run_query("QDI")
        time.sleep(1)
        if len(result) > 0:  # add check condition and publish
            if result != self.notAck:
                try:
                    pattern = re.compile(
                        r'(\d{3}\.\d\s)(\d{2}\.\d\s)(\d{4}\s)(\d{2}\.\d\s){4}(\d{2}\s)(\d\s){14}(\d{2}\.\d\s)(\d\s)(\d)')
                    matches = pattern.search(result)
                    result = matches.group()
                    timestamp = self.custolib_utils.datatimestamp()
                    result = (selfInverterId, timestamp, result)
                    result_a = ' '.join(result)
                    result_b = result_a.split(" ")
                    result_c = eg3_for(
                        self.parser.deviceDefaultSetting, result_b)
                    result_d = json.dumps(result_c, indent=2)
                    print result_d
                    # # self.client.publish('data/defaultsettings/v2', result_d, 1)
                except:
                    pass
        else:
            timestamp = self.custolib_utils.datatimestamp()
            errorId = (selfInverterId, timestamp, self.errorCode)
            errorId_a = ' '.join(errorId)
            errorId_b = errorId_a.split(" ")
            errorId_c = eg3_for(self.parser.errorLogs, errorId_b)
            errorId_d = json.dumps(errorId_c, indent=2)
            print errorId_d
            # # self.client.publish('data/errorlogs/v2', errorId_d, 1)

    def currentSettings(self, InverterId):
        result = self.run_query("QPIRI")
        time.sleep(1)
        if len(result) > 0:  # add check condition and publish
            if result != self.notAck:
                try:
                    pattern = re.compile(
                        r'(\d{3}\.\d\s\d{2}\.\d\s){2}(\d{2}\.\d\s)(\d{4}\s){2}(\d{2}\.\d\s){5}(\d{1}\s)(\d{2}\s)(\d{3}\s)(\d{1}\s){4}(\d{2}\s)(\d{1}\s){2}(\d{2}\.\d\s)(\d{1}\s){2}(\d{3})')
                    matches = pattern.search(result)
                    result = matches.group()
                    timestamp = self.custolib_utils.datatimestamp()
                    result = (selfInverterId, timestamp, result)
                    result_a = ' '.join(result)
                    result_b = result_a.split(" ")
                    result_c = eg3_for(
                        self.parser.deviceSpecification, result_b)
                    result_d = json.dumps(result_c, indent=2)
                    print result_d
                    # # self.client.publish('data/currentsettings/v2', result_d, 1)
                except:
                    pass
        else:
            timestamp = self.custolib_utils.datatimestamp()
            errorId = (selfInverterId, timestamp, self.errorCode)
            errorId_a = ' '.join(errorId)
            errorId_b = errorId_a.split(" ")
            errorId_c = eg3_for(self.parser.errorLogs, errorId_b)
            errorId_d = json.dumps(errorId_c, indent=2)
            print errorId_d
            # # self.client.publish('data/errorlogs/v2', errorId_d, 1)

    def run_query(self, command):
        delimeter = ""
        result = "nil"

        cmd_in_hex = command.encode("hex")
        cmd_in_crc = hex(crc(command))
        cmd_in_crc = cmd_in_crc[2:]
        end_char = "0D"

        command = (cmd_in_hex, cmd_in_crc, end_char)
        command = delimeter.join(command)
        try:
            ser.write(unhexlify(command))
            time.sleep(1)
            result = ser.read(200)
            result = custolib_utils().remove_non_ascii(result)
            # print result
            return result
        except serial.SerialException:
            sys.exit('Serial Failure, SerialException')
        except ValueError:
            sys.exit('Serial Failure, ValueError')
        except TypeError:
            sys.exit('Serial Failure, TypeError')
        except:
            sys.exit('Serial Failure')
