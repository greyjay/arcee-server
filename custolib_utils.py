import ConfigParser
from datetime import datetime
Config = ConfigParser.ConfigParser()
# Config.read("configfile.ini")

class custolib_utils:
	def __init__(self):
		pass

	def datatimestamp(self):
		timestamp = datetime.now().replace(microsecond=0).isoformat()
		return timestamp

	def getdaykey(self):
		timestamp =datetime.now().strftime("%Y-%m-%d")
		return timestamp

	def gettimekey(self):
		timestamp =datetime.now().strftime("%H:%M")
		return timestamp

	def remove_non_ascii(self, text):
		return ''.join([x for x in text if ord(x) < 128])






